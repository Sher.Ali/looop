<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public function allDiscounts(){
    	return $this->hasMany(RestaurantDiscount::class,'restaurant_id','id');
    }

    protected $hidden = [
        'discount_code',
    ];
}
