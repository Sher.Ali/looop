<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->user_type == "0" || Auth::user()->user_type == "1" || Auth::user()->user_type == "2"){
                return $next($request);
            }else{
                Auth::logout();
                return redirect('/')->withErrors(['You are Allow to Login']);
            }
        }else{
            Auth::logout();
            return redirect('/');
        }
    }
}
