<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Restaurant;
use App\RestaurantBranch;
use App\RestaurantDiscount;
use Redirect;
use Carbon;
use App\User;
use File;
use Auth;
use Charts;
use App\UserDiscount;
use Hash;
use App\Message;
use App\Conversation;
use App\Order;

class AdminController extends Controller
{

	public function login_user(Request $request){
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
			$user = User::find(Auth::id());
			$user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
			$user->update();
		if(Auth::user()->user_type == 0){
    		$user_role = "Super Admin";
    	}elseif(Auth::user()->user_type == 1){
    		$user_role = "Restaurant Admin";
    	}elseif(Auth::user()->user_type == 2){
    		$user_role = "Branch Admin";
    	}
			return redirect('index')->with('user_role',$user_role);
        } 
        else{
        	return Redirect::back()->withErrors(['User not matched']);
        }
	}

    public function restaurants(){
    	$data['user_role'] = "Super Admin";
    	$data['restaurants'] = Restaurant::all();
    	return view('admin.restaurants',$data);
    }

    public function createRestaurant(Request $request){
  
    	if(Auth::user()->user_type == 0){
    		$data['user_role'] = "Super Admin";
    	}elseif(Auth::user()->user_type == 1){
    		$data['user_role'] = "Restaurant Admin";
    	}elseif(Auth::user()->user_type == 2){
    		$data['user_role'] = "Branch Admin";
    	}

    	$restaurant = new Restaurant;
    	$restaurant->name                   = $request->name;
    	$restaurant->description            = $request->description;
        // $restaurant->cuisine_discription    = $request->cuisine_discription;
        $restaurant->opening                = $request->opening_timming;
        $restaurant->closing                = $request->closing_timming;
        $restaurant->instagram_link         = $request->instagram_link;
        $restaurant->phone_number           = $request->phone_number;
        $restaurant->location_link          = $request->location_link;
        $restaurant->branch                 = $request->branch;
        $restaurant->minimum_order          = $request->minimum_order;
        $restaurant->restaurant_type        = $request->restaurant_type;
        $restaurant->terms_condition        = $request->terms_condition;
        
         if($request->hasFile('image')) {
            $path = public_path('/images');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $image = $request->file('image');
            $file = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $file);
            $restaurant->image = $file;
            $restaurant->is_featured = 1;
        }

        if ($request->hasFile('logo')) {
            $path = public_path('/logo');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $image = $request->file('logo');
            $file = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/logo');
            $image->move($destinationPath, $file);
            $restaurant->logo = $file;
        }
    	$restaurant->save();
    	return Redirect::back()->withErrors(['Restaurant Create Successfully']);
    }

    public function createUser(Request $request){
    	//dd($request->all());
        if($request->user_type == 2){
            $restaurant =RestaurantBranch::where('id',$request->restaurant_branch_id)->first();
        }else{
    	   $restaurant =Restaurant::where('id',$request->restaurant_id)->first();
        }
    	$user = User::where('email',$request->email)->first();
    	if($user){
			return Redirect::back()->withErrors(['User Already Exist']);
    	}else{
    		$user = new User;
    		$user->name = $request->name;
		    $user->email = $request->email;
	        $user->password = bcrypt($request->password);
		    $user->phone_number = $request->phone_number;
		    $user->user_type = $request->user_type;
		    $user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		    $user->save();
		    $user_id = $user->id;
		    $restaurant->user_id = $user_id;
		    $restaurant->update(); 
		    return Redirect::back()->withErrors(['Restaurant Create Successfully']);
    	}
    }

    public function branches(){
    	if(Auth::user()->user_type == 0){
    		$data['user_role'] = "Super Admin";
    	}elseif(Auth::user()->user_type == 1){
    		$data['user_role'] = "Restaurant Admin";
    	}elseif(Auth::user()->user_type == 2){
    		$data['user_role'] = "Branch Admin";
    	}
    	$restaurant = Restaurant::where('user_id',Auth::user()->id)->first();
    	$data['branches'] = RestaurantBranch::where('restaurant_id',$restaurant->id)->get();
    	return view('admin.branches',$data);
    }

    public function createBranch(Request $request){
    	$restaurant = Restaurant::where('user_id',Auth::user()->id)->first();
    	$branch = new RestaurantBranch;
    	$branch->name = $request->name;
    	$branch->restaurant_id = $restaurant->id;
    	$branch->location = $request->location;
    	$branch->timming = $request->timming;
    	$branch->save();
    	return Redirect::back()->withErrors(['Restaurant branch Create Successfully']);
    }

    public function discount($branch_id){
    	if(Auth::user()->user_type == 0){
    		$data['user_role'] = "Super Admin";
    	}elseif(Auth::user()->user_type == 1){
    		$data['user_role'] = "Restaurant Admin";
    	}elseif(Auth::user()->user_type == 2){
    		$data['user_role'] = "Branch Admin";
    	}
        $data['restaurant'] = Restaurant::where('id',$branch_id)->first();
    	$data['discounts'] = RestaurantDiscount::where('restaurant_id',$branch_id)->orderBy('position','asc')->get();
    	$data['branch_id'] = $branch_id;
        //dd($data);
    	return view('admin.discount',$data);
    }

    public function looop(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $data['restaurant'] = Restaurant::where('user_id',Auth::user()->id)->first();
        $data['discounts'] = RestaurantDiscount::where('restaurant_id',$data['restaurant']->id)->orderBy('position','asc')->get();
        $data['branch_id'] = $data['restaurant']->id;
        //dd($data);
        return view('admin.discount',$data);
    }



    public function createDiscount(Request $request){
    	//dd($request->all());
    	$discount = new RestaurantDiscount;
    	$discount->restaurant_id = $request->restaurant_id;
    	$discount->discount = $request->discount;
    	$discount->position = $request->position;
        //$discount->code = $request->code;
    	$discount->save();
    	return Redirect::back()->withErrors(['discount Create Successfully']);

    }

    public function restaurantAdmin(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $user = Auth()->user();
        $data['restaurant'] = Restaurant::where('user_id',$user->id)->first();
        //dd($data);
        return view('admin.restaurantAdmin',$data);
    }

    public function editAdminRestaurant(Request $request){
        //dd($request->all());
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $restaurant = Restaurant::where('id',$request->restaurant_id)->first();
        $restaurant->name                   = $request->name;
        $restaurant->description            = $request->description;
        // $restaurant->cuisine_discription    = $request->cuisine_discription;
        $restaurant->opening                = $request->opening_timming;
        $restaurant->closing                = $request->closing_timming;
        if(Auth::user()->user_type == 0){
            $restaurant->instagram_link         = $request->instagram_link;
            $restaurant->phone_number           = $request->phone_number;
            $restaurant->location_link          = $request->location_link;
            $restaurant->branch                 = $request->branch;
            $restaurant->minimum_order          = $request->minimum_order;
            $restaurant->restaurant_type        = $request->restaurant_type;
            $restaurant->terms_condition        = $request->terms_condition;
        }
        
            
        if($request->hasFile('image')) {
            $path = public_path('/images');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $image = $request->file('image');
            $file = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $file);
            $restaurant->image = $file;
            $restaurant->is_featured = 1;
        }

        if ($request->hasFile('logo')) {
            $path = public_path('/logo');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $image = $request->file('logo');
            $file = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/logo');
            $image->move($destinationPath, $file);
            $restaurant->logo = $file;
        }
        
        $restaurant->update();
        return Redirect::back();
    }

    public function branchDiscount(Request $request){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $branch = RestaurantBranch::where('user_id',Auth::user()->id)->first();
        $data['discounts'] = UserDiscount::where('restaurant_branch_id',$branch->id)->where('status',0)->get();
        return view('admin.userOrder',$data);

    }

    public function viewBranch($branch_id){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $data['restaurant'] = RestaurantBranch::where('id',$branch_id)->first();
        return view('admin.restaurantAdmin',$data);
    
    }

    public function viewRestaurant($restaurant_id){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $data['restaurant'] = Restaurant::where('id',$restaurant_id)->first();
        return view('admin.restaurantAdmin',$data);
    }

    public function message(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        return view('admin.message',$data);
    }

    public function support(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        return view('admin.support',$data);
    }

    public function manageProfile(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        return view('admin.manageProfile',$data);
    }

    public function createDiscountCode(Request $request){
         if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
            $restaurant = Restaurant::where('id',$request->restaurant_id)->first();
            $restaurant->discount_code = $request->code;
            $restaurant->update();
            return redirect('discount/'.$request->restaurant_id);
        }
    }

    public function users(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $data['managers'] = User::where('user_type',1)->get();
        $data['users'] = User::where('user_type',3)->get();
        return view('admin.users',$data);

    }

     public function managers(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $data['managers'] = User::where('user_type',1)->get();
        return view('admin.managers',$data);

    }

    // public function Orders(){
    //     if(Auth::user()->user_type == 0){
    //         $data['user_role'] = "Super Admin";
    //     }elseif(Auth::user()->user_type == 1){
    //         $data['user_role'] = "Restaurant Admin";
    //     }elseif(Auth::user()->user_type == 2){
    //         $data['user_role'] = "Branch Admin";
    //     }
    //     $data['orders'] = UserDiscount::all();
    //     return view('admin.orders',$data);

    // }

    public function updatePassword(Request $request){
        $current_password = User::where('id','3')->first();           
      if(Hash::check($request->oldPassword, $current_password->password))
      {           
        
        return "ok";
      }
      else
      {           
        $error = array('current-password' => 'Please enter correct current password');
        return response()->json(array('error' => $error), 400);   
      }
    }

    public function messages(){
         if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
            $data['messages'] = Message::all();
            $data['users'] = User::where('user_type',1)->get();
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
            $data['messages'] = Message::where('user_id',Auth::user()->id)->get();
            $data['users'] = User::where('user_type',0)->get();
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }

        return view('admin.message',$data);
    }

    public function getConvercation($message_id){
            $conversation = Conversation::where('message_id',$message_id)->get();
            return $conversation;
    }

    public function createMessage(Request $request){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
            $message = Message::where('user_id',$request->senderTo)->first();
            if($message){
                $conversation = New Conversation;
                $conversation->message_id = $message->id;
                $conversation->sender_id = Auth::user()->id;
                $conversation->message = $request->message;
                $conversation->save();
            }else{
                $message = New Message;
                $message->user_id = $request->senderTo;
                $message->admin_id = Auth::user()->id;
                $message->save();
                $conversation = New Conversation;
                $conversation->message_id = $message->id;
                $conversation->sender_id = Auth::user()->id;
                $conversation->message = $request->message;
                $conversation->save();
            }
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
            $message = Message::where('user_id',Auth::user()->id)->first();
            if($message){
                $conversation = New Conversation;
                $conversation->message_id = $message->id;
                $conversation->sender_id = Auth::user()->id;
                $conversation->message = $request->message;
                $conversation->save();
            }else{
                $message = New Message;
                $message->user_id = $request->senderTo;
                $message->admin_id = Auth::user()->id;
                $message->save();
                $conversation = New Conversation;
                $conversation->message_id = $message->id;
                $conversation->sender_id = Auth::user()->id;
                $conversation->message = $request->message;
                $conversation->save();
            }
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        return redirect('messages');
        
    }

    public function newMessage(Request $request){
        //dd($request->all());
                if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
                $conversation = New Conversation;
                $conversation->message_id = $request->message_id;
                $conversation->sender_id = Auth::user()->id;
                $conversation->message = $request->message;
                $conversation->save();
            
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
                $conversation = New Conversation;
                $conversation->message_id = $request->message_id;
                $conversation->sender_id = Auth::user()->id;
                $conversation->message = $request->message;
                $conversation->save();
            
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        return redirect('messages');
    }

    public function Orders(Request $request){

             if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
                $data['orders'] = Order::all();
            
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
            $restaurant = Restaurant::where('user_id',Auth::user()->id)->first();
               $data['orders'] = Order::where('restaurant_id',$restaurant->id)->get(); 
            
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        return view('admin.orders',$data);

    }

    public function changePassword(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->old_password])) {
            $user = User::find(Auth::id());
            $user->password = bcrypt($request->new_password);
            $user->update();
            return Redirect::back()->withErrors(['Password Changed Successfully']);
        }else{
            return Redirect::back()->withErrors(['User not matched']);
            
        }
    }

    public function featured(){
        if(Auth::user()->user_type == 0){
            $data['user_role'] = "Super Admin";
        }elseif(Auth::user()->user_type == 1){
            $data['user_role'] = "Restaurant Admin";
        }elseif(Auth::user()->user_type == 2){
            $data['user_role'] = "Branch Admin";
        }
        $data['restaurants'] = Restaurant::where('is_featured',0)->get();
        $data['featured'] = Restaurant::where('is_featured',1)->get();
        return view('admin.featured',$data);
    }

    public function createFeaturedRestaurant(Request $request){
        $restaurant = Restaurant::where('id',$request->restaurant_id)->first();
        if($request->hasFile('image')) {
            $path = public_path('/images');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $image = $request->file('image');
            $file = time() . '_' . $image->getClientOriginalName();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $file);
            $restaurant->image = $file;
            $restaurant->is_featured = 1;
        }
        $restaurant->is_featured = 1;
        $restaurant->update(); 
        return Redirect::back()->withErrors(['Restaurant Is Featured  Successfully']);
    }

}
