<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon;
use Auth;
use App\User;
use App\Restaurant;
use App\RestaurantBranch;
use App\RestaurantDiscount;
use App\UserDiscount;
use App\Message;
use App\Conversation;
use App\Order;

class ApiController extends Controller
{
    public function registerUser(Request $request){
    	$user = User::where('phone_number',$request->phone_number)->first();
    	if($user){
			$response = array('status' =>false, 'error'=>"User Already Exist");
			return $response;
    	}else{
    		$user = User::where('email',$request->email)->first();
    		if($user){
    			$response = array('status' =>false, 'error'=>"Email Already Exist");
				return $response;
    		}else{
	    		$user = new User;
	    		$user->name = $request->name;
			    $user->email = $request->email;
			    $user->phone_number = $request->phone_number;
			    $user->gender = $request->gender;
			    $user->date_of_birth = $request->date_of_birth;
			    $user->user_type = 3;
			    $user->phone_number_verified = 1;
			    $user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
			    $user->save();
			    $response = array('status' =>true, 'data'=>$user);
				return $response;
			}
    	}
    }

    public function activeUser(Request $request){
    	$user = User::where('id',$request->id)->first();
    	if($user){
    		$user->phone_number_verified = 1;
			$user->update();
	 		$response = array('status' =>true, 'data'=>$user);
	 		return $response;
    	}else{
    		$response = array('status' =>false, 'error'=>"User Not Found");
			return $response;
    	}
    }

    public function loginUser(Request $request){
    	$user = User::where('phone_number',$request->phone_number)->first();
    	if($user){
    		if($user->phone_number_verified == 1){
	    		$user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
				$user->update();
		 		$response = array('status' =>true, 'data'=>$user);
		 		return $response;
		 	}else{
		 		$response = array('status' =>true, 'error'=>"User found but not active");
			 	return $response;
		 	}
    	}else{
    		$response = array('status' =>false, 'error'=>"User Not Found");
			return $response;
    	}
		// if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
		// 	$user = User::find(Auth::id());
		// 	$user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		// 	$user->update();
	 //        $response = array('status' =>true, 'data'=>$user);
		// 	return $response;
  //       } 
  //       else{
		// 	$response = array('status' =>false, 'error'=>"User Not Found");
		// 	return $response;
  //       }
    }

    public function logout(Request $request){
    	$user = User::where('id',$request->id)->first();
    	if($user){
    		$user->phone_number_verified = 0;
    		$user->api_token = "";
			$user->update();
	 		$response = array('status' =>true, 'error'=>"User logout");
	 		return $response;
    	}else{
    		$response = array('status' =>false, 'error'=>"User Not Found");
			return $response;
    	}
    }

    public function loginUserSocialMedia(Request $request){
    
		if($request->type=='facebook') {
		    $user=User::where('fb_email',$request->email)->first();
		    if($user){
		    	$user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		    	$user->update();
				$response = array('status' =>true, 'data'=>$user);
				return $response;
			} 
		     else{
		        $user=new User;
		        $user->fb_email=$request->email;
		        $user->name=$request->name;
		        $user->password = bcrypt('12345678');
		        $user->user_type = 3;
		        $user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		        $user->save();
		        $response = array('status' =>true, 'data'=>$user);
				return $response;
		     }
		}elseif($request->type=='google'){
		    $user=User::where('google_email',$request->email)->first();
		    if($user){
		    	$user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		    	$user->update();
				$response = array('status' =>true, 'data'=>$user);
				return $response;
			}else{
		        $user=new User;
		        $user->google_email=$request->email;
		        $user->name = $request->name;
		        $user->password = bcrypt('12345678');
		        $user->user_type = 3;
		        $user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		        $user->save();
		        $response = array('status' =>true, 'data'=>$user);
				return $response;
		    }       
		}elseif($request->type=='twitter'){
		    $user=User::where('twitter_email',$request->email)->first();
		    if($user){
		    	$user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		    	$user->update();
				$response = array('status' =>true, 'data'=>$user);
				return $response;
			}else{
		        $user=new User;
		        $user->twitter_email=$request->email;
		        $user->name=$request->name;
		        $user->password = bcrypt('12345678');
		        $user->user_type = 3;
		        $user->api_token = Str::random(60).(strtotime(Carbon\Carbon::now()));
		        $user->save();
		        $response = array('status' =>true, 'data'=>$user);
				return $response;
		    }       
		}	    
	}

	public function editProfile(Request $request){
		$user = User::where('api_token',$request->api_token)->first();
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone_number = $request->phone_number;
		$user->phone_number = $request->phone_number;
		$user->gender = $request->gender;
		$user->date_of_birth = $request->date_of_birth;
		$user->update();
		$response = array('status' =>true, 'data'=>$user);
		return $response;
	}

	public function changePassword(Request $request){
		if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
			$user->password = bcrypt($request->newPassword);
			$user->update();
			$response = array('status' =>true, 'data'=>$user);
			return $response;
		}else{
			$response = array('status' =>false, 'error'=>"User Not Found");
			return $response;
		}
	}

	public function phoneVerification(Request $request){
		$user = User::where('api_token',$request->api_token)->first();
		$user->phone_number_verified = $request->phone_number_verified;
		$user->update();
		$response = array('status' =>true, 'data'=>$user);
		return $response;
	}

	public function listOfRestaurants(){
		$data = Restaurant::with('allDiscounts')->orderBy('id', 'desc')->get();
		$response = array('status' =>true, 'data'=>$data);
		return $response;
	}

	public function listOfFeaturedRestaurants(){
		$data =Restaurant::where('is_featured',1)->orderBy('id', 'desc')->get();
		$response = array('status' =>true, 'data'=>$data);
		return $response;
	}

	public function listOfBranches(Request $request){
		$data['Branchs'] = RestaurantBranch::where('restaurant_id',$request->restaurant_id)->get();
		$data['Discount'] = RestaurantDiscount::where('restaurant_id',$request->restaurant_id)->get();
		$response = array('status' =>true, 'data'=>$data);
		return $response;	
	}

	public function listOfDiscounts(Request $request){
		$data = RestaurantDiscount::where('restaurant_branch_id',$request->restaurant_branch_id)->get();
		$response = array('status' =>true, 'data'=>$data);
		return $response;
	}

		public function applyforDiscount(Request $request){
		//dd($request->all());
		$restaurant = Restaurant::where('discount_code', $request->code)->first();
		if($restaurant){
			$discount = UserDiscount::where('user_id',$request->user_id)->where('restaurant_id',$restaurant->id)->first();
			$order = new Order;
			$order->user_id = $request->user_id;
			$order->restaurant_id = $restaurant->id;
			$order->restaurant_discount_id = $request->restaurant_discount_id;
			$order->bill = $request->bill;
			$order->save();
			
			if($discount){
				//dd($discount);
				$discount->user_id = $request->user_id;
				$discount->restaurant_id = $restaurant->id;
				$discount->restaurant_discount_id = $request->restaurant_discount_id;
				$lastLoop = RestaurantDiscount::where('restaurant_id',$restaurant->id)->orderBy('position', 'DESC')->first();
				if($lastLoop->id == $request->restaurant_discount_id){
					$discounts = UserDiscount::where('user_id',$request->user_id)->where('restaurant_id',$restaurant->id)->first();
					$discounts->delete();
				}else{
					$lastLoop = RestaurantDiscount::where('id',$request->restaurant_discount_id)->first();
					$discount->next_looop_position = ($lastLoop->position + 1);
					$loop = RestaurantDiscount::where('position',$discount->next_looop_position)->where('restaurant_id',$restaurant->id)->first();
					$discount->next_restaurant_discount_id = ($loop->id);
				}
				$discount->update();
				$userDiscount = UserDiscount::where('user_id',$request->user_id)->with('restaurant')->with('currentDisount')->with('allDiscount')->get();
				$response = array('status' =>true, 'data'=>$userDiscount );
				return $response;

			}else{
				//dd("here");
				$discount = new UserDiscount;
				$discount->user_id = $request->user_id;
				$discount->restaurant_id = $restaurant->id;
				$discount->restaurant_discount_id = $request->restaurant_discount_id;
				$lastLoop = RestaurantDiscount::where('restaurant_id',$restaurant->id)->orderBy('position', 'DESC')->first();
				if($lastLoop->id == $request->restaurant_discount_id){
					$discounts = UserDiscount::where('user_id',$request->user_id)->where('restaurant_id',$restaurant->id)->first();
					$discounts->delete();
				}else{
					$lastLoop = RestaurantDiscount::where('id',$request->restaurant_discount_id)->first();
					$discount->next_looop_position = ($lastLoop->position + 1);
					$loop = RestaurantDiscount::where('position',$discount->next_looop_position)->where('restaurant_id',$restaurant->id)->first();
					$discount->next_restaurant_discount_id = ($loop->id);
				}
				$restaurantDisount = RestaurantDiscount::where('id',$request->restaurant_discount_id)->first();
				$discount->valid_date =  date('Y-m-d', strtotime('+'.$restaurantDisount->valid_days. 'days'));
				$discount->save();
				$userDiscount = UserDiscount::where('user_id',$request->user_id)->with('restaurant')->with('currentDisount')->with('allDiscount')->get();
				$response = array('status' =>true, 'data'=>$userDiscount );
				return $response;
			}
			

		}else{
			$response = array('status' =>false, 'error'=>"Code not Match");
			return $response;
		}

	}
	public function completeLooop(Request $request){
		$discount = UserDiscount::where('user_id',$request->user_id)->where('restaurant_id',$request->restaurant_id)->first();
		if($discount){
			$discount->delete();
			$userDiscount = UserDiscount::where('user_id',$request->user_id)->with('restaurant')->with('currentDisount')->with('allDiscount')->get();
			$response = array('status' =>true, 'data'=>$userDiscount );
			return $response;
		}else{
			$response = array('status' =>false, 'error'=>"Looop not Match");
			return $response;
		}

	}

	public function listOfUserDiscounts(Request $request){
		$userDiscount = UserDiscount::where('user_id',$request->user_id)->with('restaurant')->with('currentDisount')->with('allDiscount')->get();
		$response = array('status' =>true, 'data'=>$userDiscount);
		return $response;
	}

	public function sendMessage(Request $request){
		$message = Message::where('user_id',$request->user_id)->first();
		if($message){
			$conversation = New Conversation;
			$conversation->message_id = $message->id;
			$conversation->sender_id = $request->user_id;
			$conversation->message = $request->message;
			$conversation->save();
		}else{
			$message = New Message;
			$message->user_id = $request->user_id;
			$message->admin_id = 3;
			$message->save();
			$conversation = New Conversation;
			$conversation->message_id = $message->id;
			$conversation->sender_id = $request->user_id;
			$conversation->message = $request->message;
			$conversation->save();
		}

		$response = array('status' =>true, 'data'=>'Message sended');
				return $response;
	}



	// 	public function applyforDiscount(Request $request){
	// 	//dd($request->all());
	// 	$restaurant = Restaurant::where('discount_code', $request->code)->first();
	// 	if($restaurant){
	// 		$discount = UserDiscount::where('user_id',$request->user_id)->where('restaurant_id',$restaurant->id)->first();
	// 		$order = new Order;
	// 		$order->user_id = $request->user_id;
	// 		$order->restaurant_id = $restaurant->id;
	// 		$order->restaurant_discount_id = $request->restaurant_discount_id;
	// 		$order->bill = $request->bill;
	// 		$order->save();
	// 		if($discount){
	// 			$discount->user_id = $request->user_id;
	// 			$discount->restaurant_id = $restaurant->id;
	// 			$discount->restaurant_discount_id = $request->restaurant_discount_id;
	// 			$restaurantDisount = RestaurantDiscount::where('id',$request->restaurant_discount_id)->first();
	// 			$discount->valid_date =  date('Y-m-d', strtotime('+'.$restaurantDisount->valid_days. 'days'));
	// 			$discount->update();
	// 			$userDiscount = UserDiscount::where('user_id',$request->user_id)->with('restaurant')->with('currentDisount')->with('allDiscount')->get();
	// 			$response = array('status' =>true, 'data'=>$userDiscount );
	// 			return $response;
	// 		}else{
	// 			//dd("here");
	// 			$discount = new UserDiscount;
	// 			$discount->user_id = $request->user_id;
	// 			$discount->restaurant_id = $restaurant->id;
	// 			$discount->restaurant_discount_id = $request->restaurant_discount_id;
	// 			$restaurantDisount = RestaurantDiscount::where('id',$request->restaurant_discount_id)->first();
	// 			$discount->valid_date =  date('Y-m-d', strtotime('+'.$restaurantDisount->valid_days. 'days'));
	// 			$discount->save();
	// 			$userDiscount = UserDiscount::where('user_id',$request->user_id)->with('restaurant')->with('currentDisount')->with('allDiscount')->get();
	// 			$response = array('status' =>true, 'data'=>$userDiscount );
	// 			return $response;
	// 		}
			

	// 	}else{
	// 		$response = array('status' =>false, 'error'=>"Code not Match");
	// 		return $response;
	// 	}

	// }

}
