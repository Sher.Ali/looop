<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     public function userName(){
    	return User::where('id',$this->user_id)->first();
    } 

    public function resturantName(){
    	return Restaurant::where('id',$this->restaurant_id)->first();
    } 
}
