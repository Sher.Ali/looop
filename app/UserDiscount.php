<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDiscount extends Model
{
    public function restaurant(){
    	return $this->hasOne(Restaurant::class,'id','restaurant_id');
    }

    public function currentDisount(){
    	return $this->hasOne(RestaurantDiscount::class,'id','next_restaurant_discount_id');
    } 

    public function allDiscount(){
    	return $this->hasMany(RestaurantDiscount::class,'restaurant_id','restaurant_id');
    }   
}
