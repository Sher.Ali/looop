<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantBranch extends Model
{
    public function allDiscounts(){
    	return $this->hasMany(RestaurantDiscount::class,'restaurant_id','restaurant_id');
    }
}
