<?php
use App\Restaurant;
use App\RestaurantBranch;
use App\RestaurantDiscount;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	
    return view('welcome');
});


Route::get('index', function () {
    $data['state'] = "Active";
	if(Auth::user()->user_type == 0){
    		$data['user_role'] = "Super Admin";
    	}elseif(Auth::user()->user_type == 1){
    		$data['user_role'] = "Restaurant Admin";
    	}elseif(Auth::user()->user_type == 2){
    		$data['user_role'] = "Branch Admin";
    	}
    	$data['users'] = User::all()->count();
    	$data['restaurants'] = Restaurant::all()->count();
    	$data['branches'] = RestaurantBranch::all()->count();
    	$data['discounts'] = RestaurantDiscount::all()->count();
        $users = User::all();
      
        $data['chart'] = Charts::database($users, 'line', 'highcharts')
                  ->title("Monthly new Register Users")
                  ->elementLabel("Total Users")
                  ->dimensions(1000, 500)
                  ->responsive(false)
                  ->groupByMonth();
    return view('admin.index',$data);
});

Route::get('/logout_user', function(){
	Auth::logout();
	return view('welcome');
});


Auth::routes();

Route::get('restaurants','AdminController@restaurants');
Route::post('create_restaurant','AdminController@createRestaurant');
Route::get('users','AdminController@users');
Route::get('managers','AdminController@managers');
Route::get('featured','AdminController@featured');
Route::post('login_user','AdminController@login_user');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/branches', 'AdminController@branches');
Route::post('create_user','AdminController@createUser');
Route::post('create_branch','AdminController@createBranch');
Route::get('discount/{branch_id}','AdminController@discount');
Route::post('create_discount','AdminController@createDiscount');
Route::get('restaurant_admin','AdminController@restaurantAdmin');
Route::post('edit_admin_restaurant','AdminController@editAdminRestaurant');
Route::get('branch_discount','AdminController@branchDiscount');
Route::get('make_user','AdminController@makeUser');
Route::get('viewBranch/{branch_id}','AdminController@viewBranch');
Route::get('viewRestaurant/{branch_id}','AdminController@viewRestaurant');
Route::post('/create_discount_code','AdminController@createDiscountCode');
Route::get('/update_password','AdminController@updatePassword');
Route::get('/manage_profile','AdminController@manageProfile');
Route::post('/create_featured','AdminController@createFeaturedRestaurant');

Route::get('/discount','AdminController@looop');
Route::get('/orders','AdminController@orders');
Route::get('/messages','AdminController@messages');
Route::get('/get_convercation/{message_id}','AdminController@getConvercation');
Route::post('/newMessage','AdminController@newMessage');
Route::post('/createMessage','AdminController@createMessage');
Route::post('/change_password','AdminController@changePassword');