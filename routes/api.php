<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register_user','ApiController@registerUser');
Route::post('login_user','ApiController@loginUser');
Route::post('login_with_social_media','ApiController@loginUserSocialMedia');
Route::post('edit_profile','ApiController@editProfile');
Route::post('phone_verification','ApiController@phoneVerification');
Route::post('change_password','ApiController@changePassword');
Route::get('list_of_restaurants','ApiController@listOfRestaurants');
Route::get('list_of_featured_restaurants','ApiController@listOfFeaturedRestaurants');
Route::post('list_of_branches','ApiController@listOfBranches');
Route::post('list_of_discounts','ApiController@listOfDiscounts');
Route::post('apply_for_discount','ApiController@applyforDiscount');
Route::post('active_user','ApiController@activeUser');
Route::post('logout','ApiController@logout');
Route::post('list_of_user_discounts','ApiController@listOfUserDiscounts');
Route::post('send_message','ApiController@sendMessage');
Route::post('complete_looop','ApiController@completeLooop');
