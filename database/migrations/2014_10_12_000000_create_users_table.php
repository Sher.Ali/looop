<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->string('fb_email')->nullable();
            $table->string('google_email')->nullable();
            $table->string('twitter_email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('user_type')->nullable(); // 0 Super Admin, 1 restaurant Admin, 2 branch admin, 3 user 
            $table->string('phone_number_verified')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('api_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
