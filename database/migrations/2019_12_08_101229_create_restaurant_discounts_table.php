<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('restaurant_branch_id');
            $table->foreign('restaurant_branch_id')->references('id')->on('restaurant_branches');
            $table->string('discount');
            $table->string('valid_days')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_discounts');
    }
}
