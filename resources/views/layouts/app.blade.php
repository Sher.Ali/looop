<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Looop</title>



  <link href="{{url('public/css/table-responsive.css')}}" rel="stylesheet" />
  
  <!-- Bootstrap core CSS -->
  <link href="{{url('public/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('public/css/bootstrap-reset.css')}}" rel="stylesheet">
  <!--external css-->
  <link href="{{url('public/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
  <link href="{{url('public/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css" media="screen"/>
  <link rel="stylesheet" href="{{url('css/owl.carousel.css')}}" type="text/css">

  <link rel="stylesheet" type="text/css" href="{{url('public/assets/bootstrap-datetimepicker/css/datetimepicker.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/assets/bootstrap-datepicker/css/datepicker.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/assets/select2/css/select2.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{url('public/assets/jquery-multi-select/css/multi-select.css')}}" />


  <!--dynamic table-->
  <link href="{{url('public/assets/advanced-datatable/media/css/demo_page.css')}}" rel="stylesheet" />
  <link href="{{url('public/assets/advanced-datatable/media/css/demo_table.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{url('public/assets/data-tables/DT_bootstrap.css')}}" />

  <!--right slidebar-->
  <link href="{{url('public/css/slidebars.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->

  <link href="{{url('public/css/style.css')}}" rel="stylesheet">
  <link href="{{url('public/css/style-responsive.css')}}" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    {{-- <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=sawtjot53xgp9u9vw8caug9dm9o7grtbr45r6y7i2uhdmz2c"></script> --}}
{{-- <script src="https://cdn.tiny.cloud/1/sawtjot53xgp9u9vw8caug9dm9o7grtbr45r6y7i2uhdmz2c/tinymce/5/tinymce.min.js"></script> --}}

  </head>

  <body>

    <section id="container">
      <!--header start-->
      <header class="header white-bg">
        <div class="sidebar-toggle-box">
          <i class="fa fa-bars"></i>
        </div>
        <!--logo start-->
        <a href="/index" class="logo">Looop</a>
        <!--logo end-->
        <div class="top-nav ">
          <!--search & user info start-->
          <ul class="nav pull-right top-menu">

            <!-- user login dropdown start-->
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="logout"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout
              </a>

              <form id="logout-form" action="{{url('/logout_user')}}" method="get" style="display: none;">
               @csrf
             </form>
           </div>
         </li>

         <!-- user login dropdown end -->
       </ul>
       <!--search & user info end-->
     </div>
   </header>
   <!--header end-->
   <!--sidebar start-->
   
   <aside>
    <div id="sidebar"  class="nav-collapse ">

      <!-- sidebar menu start-->
      <ul class="sidebar-menu" id="nav-accordion">

        <li>
          <a  href="{{url('/index')}}">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li>
      {{-- @php dd($user_role) @endphp --}}
       @if($user_role == "Super Admin")
        <li>
          <a href="{{url('/restaurants')}}">
            <i class="fa fa-users"></i>
            <span>Restaurants</span>
          </a>
        </li>
        <li>
          <a href="{{url('/users')}}">
            <i class="fa fa-users"></i>
            <span>Users</span>
          </a>
        </li>
        <li>
          <a href="{{url('/managers')}}">
            <i class="fa fa-users"></i>
            <span>Managers</span>
          </a>
        </li>
        <li>
          <a href="{{url('/featured')}}">
            <i class="fa fa-users"></i>
            <span>Featured</span>
          </a>
        </li> 
        <li>
          <a href="{{url('/orders/')}}">
            <i class="fa fa-users"></i>
            <span>Orders</span>
          </a>
        </li>
        <li class="sub-menu">            
          <span>Help</span>
        </li>
        
        <li><a href="{{url('/messages')}}"><i class=" fa fa-suitcase"></i>Messages</a></li>
        <li><a href="{{url('/manage_profile')}}"><i class="fa fa-cog"></i> Manage Profile</a></li>
        {{-- <li><a href="{{url('/support')}}"><i class="fa fa-bell-o"></i> Support</a></li> --}}
        <li><a href="{{url('/logout_user')}}"><i class="fa fa-key"></i>Sign Out</a></li>
                    
        
      @endif
      @if($user_role == "Restaurant Admin")
        <li>
          <a href="{{url('/restaurant_admin')}}">
            <i class="fa fa-user"></i>
            <span>Restaurant Admin</span>
          </a>
        </li>
        <li>
          <a href="{{url('/discount/')}}">
            <i class="fa fa-user"></i>
            <span>Looop</span>
          </a>
        </li>
        <li>
          <a href="{{url('/branches')}}">
            <i class="fa fa-user"></i>
            <span>Branches</span>
          </a>
        </li>
        
        <li><a href="{{url('/messages')}}"><i class=" fa fa-suitcase"></i>Messages</a></li>            
      @endif
      @if($user_role == "Branch Admin")
        <li>
          <a href="{{url('/branch_discount')}}">
            <i class="fa fa-user"></i>
            <span>Branch Discount</span>
          </a>
        </li> 
      @endif
        
        
      </ul>
      <!-- sidebar menu end-->
    </div>
  </aside>


  <section id="main-content">
    <section class="wrapper">
      @yield('content')
    </section>


<script src="js/respond.min.js" ></script>
<script src="{{url('public/js/jquery.js')}}"></script>
<script src="{{url('public/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('public/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script type="text/javascript" src="{{url('public/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{url('public/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('public/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('public/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{url('public/js/jquery.sparkline.js')}}" type="text/javascript"></script>
<script src="{{url('public/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
<script src="{{url('public/js/owl.carousel.js')}}" ></script>
<script src="{{url('public/js/jquery.customSelect.min.js')}}" ></script>
<script src="{{url('public/js/respond.min.js')}}" ></script>

<!--right slidebar-->
<script src="{{url('public/js/slidebars.min.js')}}"></script>

<!--common script for all pages-->

<!--script for this page-->
<script src="{{url('public/js/sparkline-chart.js')}}"></script>
<script src="{{url('public/js/easy-pie-chart.js')}}"></script>
<script src="{{url('public/js/count.js')}}"></script>

<script src="{{url('public/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{url('public/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{ URL::asset('public/assets/jquery-multi-select/js/jquery.multi-select.js') }}"></script>
<script src="{{ URL::asset('public/assets/select2/js/select2.min.js')}}"></script>

<script class="include" type="text/javascript" src="{{url('public/js/jquery.dcjqaccordion.2.7.js')}}"></script>

<script type="text/javascript" language="javascript" src="{{url('public/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('public/assets/data-tables/DT_bootstrap.js')}}"></script>

<!--dynamic table initialization -->
<script src="{{url('public/js/dynamic_table_init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<script type="text/javascript">

  $(document).ready(function () {

    // $(".js-example-basic-multiple").select2();
    
    $("input[type='checkbox']").change(function(){
      if($(this).is(":checked")){
        $(this).parent().addClass("background"); 
      }else{
        $(this).parent().removeClass("background");  
      }
    });  

    $("input[type='radio']").change(function () {
      $('label').click(function() {
        $('label').removeClass('background');
        $(this).addClass('background');
      });
    }); 

  });
    var current = location.pathname;
    $('#nav-accordion li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href').indexOf(current) !== -1){
            $this.addClass('active');
        }
    })



 //  $(".form_datetime-component").datetimepicker({
 //   format: "dd MM yyyy H:i:s",
 //   autoclose: true,
   
 // });

  $(document).ready(function() {
    $("#owl-demo").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
      autoPlay:true

    });
  });

      //custom select box

      $(function(){
        $('select.styled').customSelect();
      });

      // $(window).on("resize",function(){
      //     var owl = $("#owl-demo").data("owlCarousel");
      //     owl.reinit();
      // });
// $("input[type='checkbox']").change(function(){
//     if($(this).is(":checked")){
//         $(this).parent().addClass("background"); 
//     }else{
//         $(this).parent().removeClass("background");  
//     }
// });

//this function will find today's date



@yield('page_js')      
</script>

</body>
</html>