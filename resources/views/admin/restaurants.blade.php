@extends('layouts.app')

@section('content')
<!-- page start-->
<style>
  * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 33.3%;
  padding: 0 10px;
  margin-bottom: 20px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>

@if($errors->any())
<h4 style="text-align: center">{{$errors->first()}}</h4>
@endif

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading">
              Employees
            </header>
           
              <div class="panel-body" style="overflow-x:auto;">
                @if($user_role == "Super Admin")
                <div class="ibox-tools">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Add Restaurant</button>
                    </div>
                @endif
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table" >

              <thead>
              <tr>
                  
                      <th>Name</th>
                      <th>Description</th>
                      <th style="width: 280px;">Action</th>
              </tr>
              </thead>
              <tbody>
              @foreach($restaurants as $restaurant)
              <tr class="gradeX">
                  <td>{{$restaurant->name}}</td>
                  <td>{{$restaurant->description}}</td>
                 
                    <td>
                      @if($restaurant->user_id == null)
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{$restaurant->id}}" style="width: 45%;margin: 2px;background-color: #70D5AD">Add Admin</button>
                      @endif
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#edit{{$restaurant->id}}" style="width: 45%;margin: 2px;background-color: #70D5AD">Edit Restaurant</button>
                      <a href="{{url('/viewRestaurant')}}/{{$restaurant->id}}"><button class="btn btn-info" style="width: 45%;margin: 2px">View</button></a>
                      <a href="{{url('/discount')}}/{{$restaurant->id}}"><button class="btn btn-info" style="width: 45%;margin: 2px;background-color: #203D4F">Looop</button></a>
                    </td>
                  
              </tr> 
              </div>
  <div class="modal fade" id="{{$restaurant->id}}" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('create_user')}}">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Restaurant</h4>
        </div>
        <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="">
          <label>Email</label>
          <input type="email" name="email" placeholder="email" class="form-control" required="">
          <label>Password</label>
          <input type="password" name="password" placeholder="Password" class="form-control" required="">
          <label>Phone Number</label>
          <input type="number" name="phone_number" placeholder="Phone Number" class="form-control" required="">
          <input type="hidden" name="user_type" value="1">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>


  <div class="modal fade" id="edit{{$restaurant->id}}" role="dialog">
    <div class="modal-dialog" style="background-color: white">
    <form method="post" action="{{url('edit_admin_restaurant')}}" enctype="multipart/form-data">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
      <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="" value="{{$restaurant->name}}">
          <label>Description</label>
          <textarea name="description" class="form-control">{{$restaurant->description}}</textarea>
          <label>Logo</label>
          <input type="file" name="logo">
          <label>Cover Image</label>
          <input type="file" name="image">
          {{-- <label>Cuisine Discription</label>
          <textarea name="cuisine_discription" class="form-control">{{$restaurant->cuisine_discription}}</textarea> --}}
          <label>Opening Timming</label>
          <input type="time" name="opening_timming" class="form-control" value="{{$restaurant->opening}}">
          <label>Closing Timming</label>
          <input type="time" name="closing_timming" class="form-control" value="{{$restaurant->closing}}">
          <label>Instagram Link</label>
          <input type="text" name="instagram_link" class="form-control" value="{{$restaurant->instagram_link}}">
          <label>Phone Number</label>
          <input type="text" name="phone_number" class="form-control" value="{{$restaurant->phone_number}}">
          <label>location link</label>
          <input type="text" name="location_link" class="form-control" value="{{$restaurant->location_link}}">
          <label>Branch</label>
          <input type="text" name="branch" class="form-control" value="{{$restaurant->branch}}">
          <label>Minimum Order</label>
          <input type="text" name="minimum_order" class="form-control" value="{{$restaurant->minimum_order}}">
          <label>Restaurant Type</label>
          <select name="restaurant_type" class="form-control">
            <option>Chose one</option>
            <option value="asian" {{$restaurant->restaurant_type == "asian"  ? 'selected' : ''}}>Asian</option>
            <option value="burgers" {{$restaurant->restaurant_type == "burgers"  ? 'selected' : ''}}>Burgers</option>
            <option value="deserts" {{$restaurant->restaurant_type == "deserts"  ? 'selected' : ''}}>Deserts</option>
            <option value="drinks" {{$restaurant->restaurant_type == "drinks"  ? 'selected' : ''}}>Drinks</option>
            <option value="fast food" {{$restaurant->restaurant_type == "fast food"  ? 'selected' : ''}}>Fast Food</option>
            <option value="healthy" {{$restaurant->restaurant_type == "healthy"  ? 'selected' : ''}}>Healthy</option>
            <option value="indian" {{$restaurant->restaurant_type == "indian"  ? 'selected' : ''}}>Indian</option>
            <option value="international" {{$restaurant->restaurant_type == "international"  ? 'selected' : ''}}>International</option>
            <option value="italian" {{$restaurant->restaurant_type == "italian"  ? 'selected' : ''}}>Italian</option>
            <option value="mexican" {{$restaurant->restaurant_type == "mexican"  ? 'selected' : ''}}>Mexican</option>
            <option value="middle-eastern" {{$restaurant->restaurant_type == "middle-eastern"  ? 'selected' : ''}}>Middle-Eastern</option>
          </select>
          <label>Terms Condition</label>
          <textarea name="terms_condition" class="form-control">{{$restaurant->terms_condition}}</textarea> 
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </div>
      </form>
    </div>
  </div>
              @endforeach
              </tbody>
              </table>
              </div>
            </div>
              <div class="modal fade" id="myModal1" role="dialog" >  
    <div class="modal-dialog">
    <form method="post" action="{{url('create_restaurant')}}" enctype="multipart/form-data">
      @csrf
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Restaurant</h4>
        </div>
        <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="">
          <label>Description</label>
          <textarea name="description" class="form-control"></textarea>
          <label>Logo</label>
          <input type="file" name="logo">
          <label>Cover Image</label>
          <input type="file" name="image">
          {{-- <label>Cuisine Discription</label>
          <textarea name="cuisine_discription" class="form-control"></textarea> --}}
          <label>Opening Timming</label>
          <input type="time" name="opening_timming" class="form-control" >
          <label>Closing Timming</label>
          <input type="time" name="closing_timming" class="form-control">
          <label>Instagram Link</label>
          <input type="text" name="instagram_link" class="form-control">
          <label>Phone Number</label>
          <input type="text" name="phone_number" class="form-control">
          <label>location link</label>
          <input type="text" name="location_link" class="form-control">
          <label>branch</label>
          <input type="text" name="branch" class="form-control">
          <label>Minimum Order</label>
          <input type="text" name="minimum_order" class="form-control">
          <label>Restaurant Type</label>
          <select name="restaurant_type" class="form-control">
            <option value="asian">Asian</option>
            <option value="burgers">Burgers</option>
            <option value="deserts">Deserts</option>
            <option value="drinks">Drinks</option>
            <option value="fast food">Fast Food</option>
            <option value="healthy">Healthy</option>
            <option value="indian">Indian</option>
            <option value="international">International</option>
            <option value="italian">Italian</option>
            <option value="mexican">Mexican</option>
            <option value="middle-eastern">Middle-Eastern</option>
          </select>
          <label>Terms Condition</label>
          <textarea name="terms_condition" class="form-control"></textarea>
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>
      
@endsection