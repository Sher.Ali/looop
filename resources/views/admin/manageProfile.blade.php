@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-lg-12" align="center">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Change Password</button>
        </div>

        <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
<form method="post" action="{{url('change_password')}}">
      <input type="hidden" name="crsf_token" value="{{@csrf_token()}}">
      <div class="modal-body">
      	<label>Email</label>
       <input type="email" name="email" class="form-control"><br>

       <label>Old Password</label>
       <input type="text" name="old_password" class="form-control"><br>

       <label>New Password</label>
       <input type="text" name="new_password" class="form-control"><br>
       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      	<button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
</form>
    </div>
  </div>
</div>
</div>      

@endsection
