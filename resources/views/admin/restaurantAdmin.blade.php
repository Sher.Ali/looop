@extends('layouts.app')
@section('content')

<label>Name</label>
<input type="text" name="name" value="{{$restaurant->name}}" class="form-control" readonly>
<br>

<label>Description</label>
<input type="text" name="name" value="{{$restaurant->description}}" class="form-control" readonly>
<br>

<label>Opening</label>
<input type="text" name="name" value="{{$restaurant->opening}}" class="form-control" readonly>
<br>

<label>Closing</label>
<input type="text" name="name" value="{{$restaurant->closing}}" class="form-control" readonly>
<br>

<label>Instagram Link</label>
<input type="text" name="instagram_link" value="{{$restaurant->instagram_link}}" class="form-control" readonly>
<br>

<label>Phone Number</label>
<input type="text" name="phone_number" value="{{$restaurant->phone_number}}" class="form-control" readonly>
<br>

<label>location link</label>
<input type="text" name="location_link" value="{{$restaurant->location_link}}" class="form-control" readonly>
<br>
      
<label>Minimum Order</label>
<input type="text" name="minimum_order" value="{{$restaurant->minimum_order}}" class="form-control" readonly>
<br>

<label>Restaurant Type</label>
<input type="text" name="restaurant_type" value="{{$restaurant->restaurant_type}}" class="form-control" readonly>
<br>

<label>Terms Condition</label>
<input type="text" name="terms_condition" value="{{$restaurant->terms_condition}}" class="form-control" readonly>
<br>
<div class="row">
  <div class="col-md-6" align="center">
    <label>Logo</label><br>
    @if($restaurant->logo != "")
    <img src="{{url('/public/logo/'.$restaurant->logo)}}" style="width: 200px;height: 200px">
    @else
    <img src="{{url('/public/img/noImage.png')}}" style="width: 200px;height: 200px">
    @endif
  </div>
  <div class="col-md-6" align="center">
    <label>Cover Image</label><br>
    @if($restaurant->image != "")
    <img src="{{url('/public/images/'.$restaurant->image)}}" style="width: 200px;height: 200px">
    @else
    <img src="{{url('/public/img/noImage.png')}}" style="width: 200px;height: 200px">
    @endif
  </div>
</div>


<br>
<div align="center">
 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{$restaurant->id}}">Edit Restaurant</button>
</div>

  <div class="modal fade" id="{{$restaurant->id}}" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('edit_admin_restaurant')}}" enctype="multipart/form-data">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Restaurant</h4>
        </div>
        <div style="padding: 5px">
        <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
      @if($user_role == "Super Admin")
      <label>Name</label>
			<input type="text" name="name" value="{{$restaurant->name}}" class="form-control">
			<br>

			<label>Description</label>
			<input type="text" name="description" value="{{$restaurant->description}}" class="form-control">
			<br>
      @else
      <label>Name</label>
      <input type="text" name="name" value="{{$restaurant->name}}" class="form-control" readonly="">
      <br>

      <label>Description</label>
      <input type="text" name="description" value="{{$restaurant->description}}" class="form-control" readonly>
      <br>
      @endif

			<label>Opening</label>
			<input type="time" name="opening_timming" value="{{$restaurant->opening}}" class="form-control">
			<br>

			<label>Closing</label>
			<input type="time" name="closing_timming" value="{{$restaurant->closing}}" class="form-control">
			<br>
      @if($user_role == "Super Admin")
      <label>Instagram Link</label>
      <input type="text" name="instagram_link" value="{{$restaurant->instagram_link}}" class="form-control" >
      <br>

      <label>Phone Number</label>
      <input type="text" name="phone_number" value="{{$restaurant->phone_number}}" class="form-control" >
      <br>

      <label>location link</label>
      <input type="text" name="location_link" value="{{$restaurant->location_link}}" class="form-control" >
      <br>
            
      <label>Minimum Order</label>
      <input type="text" name="minimum_order" value="{{$restaurant->minimum_order}}" class="form-control" >
      <br>

      <label>Restaurant Type</label>
      <select name="restaurant_type" class="form-control">
            <option>Chose one</option>
            <option value="asian" {{$restaurant->restaurant_type == "asian"  ? 'selected' : ''}}>Asian</option>
            <option value="burgers" {{$restaurant->restaurant_type == "burgers"  ? 'selected' : ''}}>Burgers</option>
            <option value="deserts" {{$restaurant->restaurant_type == "deserts"  ? 'selected' : ''}}>Deserts</option>
            <option value="drinks" {{$restaurant->restaurant_type == "drinks"  ? 'selected' : ''}}>Drinks</option>
            <option value="fast food" {{$restaurant->restaurant_type == "fast food"  ? 'selected' : ''}}>Fast Food</option>
            <option value="healthy" {{$restaurant->restaurant_type == "healthy"  ? 'selected' : ''}}>Healthy</option>
            <option value="indian" {{$restaurant->restaurant_type == "indian"  ? 'selected' : ''}}>Indian</option>
            <option value="international" {{$restaurant->restaurant_type == "international"  ? 'selected' : ''}}>International</option>
            <option value="italian" {{$restaurant->restaurant_type == "italian"  ? 'selected' : ''}}>Italian</option>
            <option value="mexican" {{$restaurant->restaurant_type == "mexican"  ? 'selected' : ''}}>Mexican</option>
            <option value="middle-eastern" {{$restaurant->restaurant_type == "middle-eastern"  ? 'selected' : ''}}>Middle-Eastern</option>
          </select>
{{-- <input type="text" name="restaurant_type" value="{{$restaurant->restaurant_type}}" class="form-control"> --}}
<br>

<label>Terms Condition</label>
<textarea name="terms_condition" class="form-control">{{$restaurant->terms_condition}}</textarea>
{{-- <input type="text" name="terms_condition" value="{{$restaurant->terms_condition}}" class="form-control"> --}}
<br>

      <div class="row" style="margin: 0px">
        <div class="col-md-6" align="center">
          <label>Logo</label>
          <input type="file" name="logo"><br>
          @if($restaurant->logo != "")
          <img src="{{url('/public/logo/'.$restaurant->logo)}}" style="width: 200px;height: 200px">
          @else
          <img src="{{url('/public/img/noImage.png')}}" style="width: 200px;height: 200px">
          @endif
        </div>
        <div class="col-md-6" align="center" >
          <label>Cover Image</label>
          <input type="file" name="image"><br>
          @if($restaurant->image != "")
          <img src="{{url('/public/images/'.$restaurant->image)}}" style="width: 200px;height: 200px">
          @else
          <img src="{{url('/public/img/noImage.png')}}" style="width: 200px;height: 200px">
          @endif
        </div>

        @endif
{{-- 
			<label>Cover Image</label><br>
			<input type="file" name="image"> --}}
			
      <br>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success"  style="margin-top: 2px">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"  style="margin-top: 2px">Close</button>
        </div>
        </div>
      </div>
      </form>
    </div>
  </div>
@endsection