@extends('layouts.app')

@section('content')
<!-- page start-->
<style>
  * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 33.3%;
  padding: 0 10px;
  margin-bottom: 20px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>

@if($errors->any())
<h4 style="text-align: center">{{$errors->first()}}</h4>
@endif

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading">
              Discount
            </header>
           
              <div class="panel-body" style="overflow-x:auto;">
                
                <div class="ibox-tools">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal12">Add Discount</button>
                   
                @if($restaurant->discount_code == null)
                
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal3">Make discount Code</button>
                   
                @endif
                 </div>
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table" >

              <thead>
              <tr>
                  
                      <th>Discount</th>
                      <th>Position</th>
                      
              </tr>
              </thead>
              <tbody>
              @foreach($discounts as $discount)
              <tr class="gradeX">
                  <td>{{$discount->discount}}</td>
                  <td>{{$discount->position}}</td>
                 
                  
              </tr> 
              </div>

              @endforeach
              </tbody>
              </table>
              </div>
            </div>
  <div class="modal fade" id="myModal12" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('create_discount')}}">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_id" value="{{$branch_id}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New discount</h4>
        </div>
        <div class="modal-body">
          <label>Discount</label>
          <input type="text" name="discount" placeholder="5%" class="form-control" required="">
          <label>Position</label>
          <input type="number" name="position" placeholder="1" class="form-control" required="" min="0">
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>

    <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('create_discount_code')}}">
      @csrf
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Discount Code</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="restaurant_id" value="{{$branch_id}}">
          <label>Code( Six digit )</label>
          <input type="text" name="code" placeholder="Six Digit Code" class="form-control" required="">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>

@endsection


