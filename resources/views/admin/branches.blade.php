@extends('layouts.app')

@section('content')
<!-- page start-->
<style>
  * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 33.3%;
  padding: 0 10px;
  margin-bottom: 20px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>

@if($errors->any())
<h4 style="text-align: center">{{$errors->first()}}</h4>
@endif

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading">
              Employees
            </header>
           
              <div class="panel-body" style="overflow-x:auto;">
                @if($user_role == "Restaurant Admin")
                <div class="ibox-tools">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Add Branches</button>
                    </div>
                @endif

                
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table" >

              <thead>
              <tr>
                  
                      <th>Name</th>
                      <th>Location</th>
                      <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @foreach($branches as $branch)
              <tr class="gradeX">
                  <td>{{$branch->name}}</td>
                  <td>{{$branch->location}}</td>
                  <td>
                    {{-- <a href="{{url('/discount')}}/{{$branch->id}}"><button class="btn btn-info">Discount</button></a> --}}
                    <a href="{{url('/viewBranch')}}/{{$branch->id}}"><button class="btn btn-info">View</button></a>
                    @if($branch->user_id == "")
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{$branch->id}}">Add Admin</button></td>
                    @endif
              </tr> 
              </div>
  <div class="modal fade" id="{{$branch->id}}" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('create_user')}}">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_branch_id" value="{{$branch->id}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New User</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="user_type" value="2">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="">
          <label>Email</label>
          <input type="email" name="email" placeholder="email" class="form-control" required="">
          <label>Password</label>
          <input type="password" name="password" placeholder="Password" class="form-control" required="">
          <label>Phone Number</label>
          <input type="number" name="phone_number" placeholder="Phone Number" class="form-control" required="">
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>
              @endforeach
              </tbody>
              </table>
              </div>
            </div>
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('create_branch')}}">
      @csrf
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Branch</h4>
        </div>
        <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="">
          <label>Location</label>
          <input type="text" name="location" placeholder="Location" class="form-control" required="">
          <label>Timming</label>
          <input type="text" name="timming" placeholder="Timming" class="form-control" required="">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>

@endsection



<script>
@section('page_js')
       
    $(function() {
        var scntDiv = $('#p_scents');
        var i = $('#p_scents').length + 1;
        
        $('#addScnt').click(function() {
          $('#modelSubmitButton').show();
                $('<p><label for="p_scnts" style="width:85%;margin-top:5px"><input type="text" class="form-control" id="p_scnt" name="location[]" required></label><a href="#" class="remScnt">Remove</a></p>').appendTo(scntDiv);
                 $('.remScnt').click(function() { 
                        $(this).parent('p').remove();
                        
                return false;
        });
        });
        
       
});
@endsection
</script>
