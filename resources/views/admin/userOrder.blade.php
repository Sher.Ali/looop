@extends('layouts.app')

@section('content')
<!-- page start-->
<style>
  * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 33.3%;
  padding: 0 10px;
  margin-bottom: 20px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>

@if($errors->any())
<h4 style="text-align: center">{{$errors->first()}}</h4>
@endif

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading">
              Employees
            </header>
           
              <div class="panel-body" style="overflow-x:auto;">
                @if($user_role == "Super Admin")
                <div class="ibox-tools">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Add Restaurant</button>
                    </div>
                @endif
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table" >

              <thead>
              <tr>
                  
                      <th>User</th>
                      <th>Restaurant Discount</th>
                      <th>Restaurant Branch</th>
                      <th style="width: 280px;">Action</th>
              </tr>
              </thead>
              <tbody>
              @foreach($discounts as $discount)
              <tr class="gradeX">
                  <td>{{$discount->user_id}}</td>
                  <td>{{$discount->restaurant_discount_id}}</td>
                  <td>{{$discount->restaurant_branch_id}}</td>
                    <td>
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{$discount->id}}">Add Admin</button>
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#edit{{$discount->id}}">Edit Restaurant</button>
                    </td>
                  
                   
              </tr> 
              </div>


              @endforeach
              </tbody>
              </table>
              </div>
            </div>
              
      
@endsection