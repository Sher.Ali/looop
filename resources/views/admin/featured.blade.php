@extends('layouts.app')

@section('content')
<!-- page start-->
<style>
  * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 33.3%;
  padding: 0 10px;
  margin-bottom: 20px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>

@if($errors->any())
<h4 style="text-align: center">{{$errors->first()}}</h4>
@endif

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading">
              Featured
            </header>
           
              <div class="panel-body" style="overflow-x:auto;">
                @if($user_role == "Super Admin")
                <div class="ibox-tools">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Add Featured Restaurant</button>
                    </div>
                @endif
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table" >

              <thead>
              <tr>
                  
                      <th>Name</th>
                      <th>Description</th>
                      {{-- <th style="width: 280px;">Action</th> --}}
              </tr>
              </thead>
              <tbody>
              @foreach($featured as $restaurant)
              <tr class="gradeX">
                  <td>{{$restaurant->name}}</td>
                  <td>{{$restaurant->description}}</td>
                 
                    {{-- <td>
                      @if($restaurant->user_id == null)
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#{{$restaurant->id}}" style="width: 45%;margin: 2px;background-color: #70D5AD">Add Admin</button>
                      @endif
                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#edit{{$restaurant->id}}" style="width: 45%;margin: 2px;background-color: #70D5AD">Edit Restaurant</button>
                      <a href="{{url('/viewRestaurant')}}/{{$restaurant->id}}"><button class="btn btn-info" style="width: 45%;margin: 2px">View</button></a>
                      <a href="{{url('/discount')}}/{{$restaurant->id}}"><button class="btn btn-info" style="width: 45%;margin: 2px;background-color: #203D4F">Looop</button></a>
                    </td> --}}
                  
              </tr> 
              </div>
              @endforeach
              </tbody>
              </table>
              </div>
            </div>
              <div class="modal fade" id="myModal1" role="dialog" >  
    <div class="modal-dialog">
    <form method="post" action="{{url('create_featured')}}" enctype="multipart/form-data">
      @csrf
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New featured Restaurant</h4>
        </div>
        <div class="modal-body">
          <label>Restaurant</label>
          <select name="restaurant_id" class="form-control" required="">
            @foreach($restaurants as $res)
            <option value="{{$res->id}}">{{$res->name}}</option>
            @endforeach
          </select>

          <label>Cover Image</label>
          <input type="file" name="image">
          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div>
      
@endsection