@extends('layouts.app')

@section('content')
<!-- page start-->
<style>
  * {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 33.3%;
  padding: 0 10px;
  margin-bottom: 20px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #f1f1f1;
}
</style>

@if($errors->any())
<h4 style="text-align: center">{{$errors->first()}}</h4>
@endif

      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
            <header class="panel-heading">
              Orders
            </header>
           
              <div class="panel-body" style="overflow-x:auto;">
                {{-- @if($user_role == "Super Admin")
                <div class="ibox-tools">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Add Restaurant</button>
                    </div>
                @endif --}}
              <div class="adv-table">
              <table  class="display table table-bordered table-striped" id="dynamic-table" >

              <thead>
              <tr>
                  
                      <th>User Name</th>
                      <th>Resturant Name</th>
                      <th>Bill</th>
                      
              </tr>
              </thead>
              <tbody>
              @foreach($orders as $order)
              <tr class="gradeX">
                  <td>{{$order->userName()->name}}</td>
                  <td>{{$order->resturantName()->name}}</td>
                  <td>{{$order->bill}}</td>
                  
              </tr> 
              </div>
 {{--  <div class="modal fade" id="{{$restaurant->id}}" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="{{url('create_user')}}">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Restaurant</h4>
        </div>
        <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="">
          <label>Email</label>
          <input type="email" name="email" placeholder="email" class="form-control" required="">
          <label>Password</label>
          <input type="password" name="password" placeholder="Password" class="form-control" required="">
          <label>Phone Number</label>
          <input type="number" name="phone_number" placeholder="Phone Number" class="form-control" required="">
          <input type="hidden" name="user_type" value="1">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div> --}}


{{--   <div class="modal fade" id="edit{{$restaurant->id}}" role="dialog">
    <div class="modal-dialog" style="background-color: white">
    <form method="post" action="{{url('edit_admin_restaurant')}}" enctype="multipart/form-data">
      @csrf
      <!-- Modal content-->
      <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
      <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="" value="{{$restaurant->name}}">
          <label>Description</label>
          <textarea name="description" class="form-control">{{$restaurant->description}}</textarea>
          <label>Logo</label>
          <input type="file" name="logo">
          <label>Cover Image</label>
          <input type="file" name="image">
          <label>Opening Timming</label>
          <input type="time" name="opening_timming" class="form-control" value="{{$restaurant->opening}}">
          <label>Closing Timming</label>
          <input type="time" name="closing_timming" class="form-control" value="{{$restaurant->closing}}">
          <label>Instagram Link</label>
          <input type="text" name="instagram_link" class="form-control" value="{{$restaurant->instagram_link}}">
          <label>Phone Number</label>
          <input type="text" name="phone_number" class="form-control" value="{{$restaurant->phone_number}}">
          <label>location link</label>
          <input type="text" name="location_link" class="form-control" value="{{$restaurant->location_link}}">
          <label>Branch</label>
          <input type="text" name="branch" class="form-control" value="{{$restaurant->branch}}">
          <label>Minimum Order</label>
          <input type="text" name="minimum_order" class="form-control" value="{{$restaurant->minimum_order}}">
          <label>Restaurant Type</label>
          <input type="text" name="restaurant_type" class="form-control" value="{{$restaurant->restaurant_type}}">
          <label>Terms Condition</label>
          <input type="text" name="terms_condition" class="form-control" value="{{$restaurant->terms_condition}}">
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
      </div>
      </form>
    </div> --}}
  </div>
              @endforeach
              </tbody>
              </table>
              </div>
            </div>
{{--               <div class="modal fade" id="myModal1" role="dialog" >  
    <div class="modal-dialog">
    <form method="post" action="{{url('create_restaurant')}}" enctype="multipart/form-data">
      @csrf
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Restaurant</h4>
        </div>
        <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" class="form-control" required="">
          <label>Description</label>
          <textarea name="description" class="form-control"></textarea>
          <label>Logo</label>
          <input type="file" name="logo">
          <label>Cover Image</label>
          <input type="file" name="image">
          <label>Cuisine Discription</label>
          <textarea name="cuisine_discription" class="form-control"></textarea>
          <label>Opening Timming</label>
          <input type="time" name="opening_timming" class="form-control" >
          <label>Closing Timming</label>
          <input type="time" name="closing_timming" class="form-control">
          <label>Instagram Link</label>
          <input type="text" name="instagram_link" class="form-control">
          <label>Phone Number</label>
          <input type="text" name="phone_number" class="form-control">
          <label>location link</label>
          <input type="text" name="location_link" class="form-control">
          <label>branch</label>
          <input type="text" name="branch" class="form-control">
          <label>Minimum Order</label>
          <input type="text" name="minimum_order" class="form-control">
          <label>Restaurant Type</label>
          <input type="text" name="restaurant_type" class="form-control">
          <label>Terms Condition</label>
          <input type="text" name="terms_condition" class="form-control">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
    </div>
  </div> --}}
      
@endsection