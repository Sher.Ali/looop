@extends('layouts.app')

@section('content')

<div class="row">

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create New Conversation</button>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create Conversation</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
<form method="post" action="createMessage">
      <!-- Modal body -->
      <div class="modal-body">
       <select name="senderTo" class="form-control">
           @foreach($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
           @endforeach
       </select>

       <label>Message</label>
       <textarea name="message" class="form-control"></textarea>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
</form>
    </div>
  </div>
</div>

<div class="col-md-12" style="border: 1px solid;text-align: center;background-color: #2a3542;height: 50px;color: white;padding-top: 5px;" >
<h4><strong> All Messages </strong></h4>

</div>
<br>

	<div class="col-md-3" >
		@foreach($messages as $message)

<a onclick="myFunction(this.id)" id="{{$message->id}}">
		<div style="background-color: white; margin: 5px;padding: 10px" >
			<h5 style="text-align: center;"><strong>{{$message->userName()->name}}</strong></h5>
		</div>
	</a>
		@endforeach
	</div>

	<div class="col-md-8" style="border: 1px solid; height: 600px">
		<div id="convercation" >
            
            
        </div>
        <div style="position: absolute;bottom: 10px;width: 100%;">
<form method="post" action="newMessage">
        <input type="hidden" id="convercationInput" name="message_id" value="">
        <div class="col-md-10">
            <textarea name="message" class="form-control"></textarea>
        </div>
        <div class="col-md-1">
            <button type="submit" class="btn btn-success" >Submit</button>
        </div>
        </div>
	</div>
</form>
</div>

@endsection
<script type="text/javascript">
@section('page_js')
function myFunction($this) {
    $("#convercationInput").val($this);
	data = '';
  $.ajax({

                    url: 'get_convercation/'+$this,
                    type:'GET',
                    success: function(response){
                     console.log('response',response);
                     
                     $.each(response,function(index,value){
                        if(value.sender_id == 3){
                            data +='<div class="col-md-12" style="text-align: right;background-color: #2a3542; color:white;height: 35px;margin:10px"><h4>'+value.message+'</h4></div>';
                        }else{
                            data +='<div class="col-md-12" style="text-align: left;background-color: white; color:black;;height: 35px;margin:10px" id=""><h4>'+value.message+'</h4></div>';
                        }
                    })
                     
                     $('#convercation').html(data);
                 },
                 error: function(data){
                    console.log(data);
                }
            });
}

@endsection 
</script>